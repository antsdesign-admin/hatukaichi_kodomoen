<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、言語、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'z110149_wordpress0002');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'z110149');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'DW8ByW3z');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!P}TQQ<02E@ixuvd4GK ,fvgR^AQOO;?UELKJ^U00-BaC8].X/}O-C/~oL7GDW m');
define('SECURE_AUTH_KEY',  'Qb|BD(8j3pnHeHzj?NRV]W{E#%;(YU>(hm-[H6]G!fqyRT6!Ox@%#E]D/6%Mp^Zm');
define('LOGGED_IN_KEY',    ':#LN|W$%-$TllO%ESX9m-L9Hoj,+eY8R$@?49H$T32ygqQPGM7&NTF#{MrJ~.PXr');
define('NONCE_KEY',        'H5Z#B=36lLFG2E!]~R#3lJ9Ub[Z13|,iwDa@g4y1XohB^rX7x]fL3$n)?CmM8^#_');
define('AUTH_SALT',        '(!3~6ym{*F~H!9-hGv7*(Q0S,CJklTK/*81(|2#bQ1itwaND4E#h*ZNz{w*3X!u@');
define('SECURE_AUTH_SALT', 'a-9oA_Fr{s1mLpz=p92_g4gB4yzqH`PgwFWQy.9}pt|QJ3}OqaIW )dyq5 bU277');
define('LOGGED_IN_SALT',   'jNS:Tn,#plu$X1e5+QiVu*kwp)EX5p@;gG)D;_|q0WmG6o&_GINcJ+*;:GesTf9g');
define('NONCE_SALT',       '&9{g9y,]Q!VBj)?u7(k6CuH.3rzCDhF!Mh#^Z(uAhF.R[]CHR}nr3FN_ o VxxDF');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_hatukaichi_';

/**
 * ローカル言語 - このパッケージでは初期値として 'ja' (日本語 UTF-8) が設定されています。
 *
 * WordPress のローカル言語を設定します。設定した言語に対応する MO ファイルが
 * wp-content/languages にインストールされている必要があります。たとえば de_DE.mo を
 * wp-content/languages にインストールし WPLANG を 'de_DE' に設定すると、ドイツ語がサポートされます。
 */
define('WPLANG', 'ja');

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
