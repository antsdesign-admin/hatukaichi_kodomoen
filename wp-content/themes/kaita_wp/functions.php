<?php 
/**
* get page name
**/
function getPageName(){
	if(is_page()){
		$pageId = get_the_ID();
		$curPage = get_page($pageId);
		$curPageParent = $curPage->post_parent;
		if($curPageParent == 0){
			$pname = $curPage->post_name;
		}else if(is_page('staff')){
            $pname = 'staff';
        }else if(is_page('issues')){
            $pname = 'issues';
        }else if(is_page('equipment')){
            $pname = 'equipment';
        }else if(is_page('community')){
            $pname = 'community';
        }else if(is_page('access')){
            $pname = 'access';
        }else if(is_page('event')){
            $pname = 'event';
        }else{
			$pname = get_page(get_top_parent_page_id())->post_name;
		}
	}
	else if(is_category() || is_single() || is_search()){
		$pname = 'topics';
    }
    else if(is_home()||is_front_page()){
        $pname = 'index';
    }
    return $pname;
}

function needRemoveP() {
	remove_filter('the_content', 'wpautop'); 
}
/**
 * shortcode
 */
function template_src() {
    return get_bloginfo('template_url');
}

function template_url() {
    return get_bloginfo('url');
}

add_shortcode('src', 'template_src');
add_shortcode('url', 'template_url');


function get_side() {
    ob_start();
    get_template_part('sidebar');
    return ob_get_clean();
}
add_shortcode('side_bar', 'get_side');



add_filter('wp_terms_checklist_args','wp_terms_checklist_args');
function wp_terms_checklist_args($args) {
    $args['checked_ontop'] = false;
    return $args;
}