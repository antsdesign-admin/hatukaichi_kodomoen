// JavaScript Document


$(document).ready(function() {
	 //まずトップに戻るを消しておきます。
	$("#back-top").hide();

	 //スクロールされたら	
	$(window).scroll(function () {
		//100pxいったら
		if ($(this).scrollTop() > 100) {
			//トップに戻るをフェードイン
			$('#back-top').fadeIn();
		//100pxいかなかったら
		} else {
			//隠す
			$('#back-top').fadeOut();
		}
	});

	 //トップに戻るをクリックしたら	
	$('#back-top a').click(function () {
		//スムーズに上に戻る
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
});

	$(function() {
	$('#nav li')
		.each(function(i){
			$(this).css('background', 'url(../img/nav/nav0'+(i+1)+'_o.jpg) no-repeat');
		})
		.find('img').hover(
			function(){  
				$(this).stop().animate({'opacity' : '0'}, 100);  
			},
			function(){
				$(this).stop().animate({'opacity' : '1'}, 500);
			}
			); 
		}); 
$(function(){
	$("a.opa").hover(function(){
		$(this).stop().animate({"opacity":"0.75"});
	},function(){
		$(this).stop().animate({"opacity":"1"});
	});
});
$(function(){
	$('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});

$(window).on('load', function () {
    if ($('.videoFancy').length) {
		$('[data-fancybox]').fancybox({
			youtube : {
				controls : 0,
				showinfo : 0
			}
		});
	}
})