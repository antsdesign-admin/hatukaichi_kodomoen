<?php get_header(); ?>
<div id="container">
    <div class="bread clearfix">
        <ul>
            <li><a href="<?php bloginfo( 'url' ); ?>">HOME</a></li>
            <li>＞</li>
            <li><a href="<?php bloginfo( 'url' ); ?>/information">新着情報</a></li>
            <li>＞</li>
            <li><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?> <?php the_title(); ?></li>
        </ul>
    </div><!--bread-->
    <div id="content" class="clearfix">
            <div class="p_ttl">
              <p><img src="<?php bloginfo( 'template_url' ); ?>/img/topics/page_ttl.png" width="966" height="46" alt="新着情報" /></p>
            </div><!--p_ttl-->
              <div id="side">
                <img class="cate_top" src="<?php bloginfo( 'template_url' ); ?>/img/topics/cate_top.png" alt=" " width="213" height="9" />
                <ul class="cate">
                <?php
                    $args = array(
                        'show_count' => 0,
                        'hide_empty' => 0,
                        'child_of' => 1,
                        'title_li' => ''
                    );
                    wp_list_categories($args); 
                ?>
                </ul>
                <?php get_sidebar(); ?>
              </div><!--side-->
 
            <div id="main_cont">
            	<div class="c_ttl"><?php
                $cat = get_the_category();
                $cat = $cat[0];
                $chekname= $cat->cat_name;
                $chekid = $cat->cat_ID;
                echo ' '.$chekname;
                ?>
                </div>
                <article>
                <article class="box clearfix">
                <div class="data"><?php the_time('Y.m.d');?></div>
                <div class="title"><?php the_title(); ?></div>
                <div class="detail">                
				<?php if(have_posts()): while(have_posts()): the_post(); ?>
                <?php the_content(); ?>
                </div>
                <?php endwhile; endif; ?>   
                </article>
                <div class="toTop_topi"><a href="#">▲PAGE TOP</a></div>
            
            </div><!--main_cont-->
	</div><!-- #content -->
<?php get_footer(); ?>