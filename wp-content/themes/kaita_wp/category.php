<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); 

$cat = get_queried_object();
$catID = $cat->term_id;
if($catID != 1){
    $catPId = $cat->parent;
    $catPname = get_category($catPId)->name;
}
?>
<!--start-contents --> 
<div id="container">
    <div class="bread clearfix">
        <ul>
            <li><a href="<?php bloginfo( 'url' ); ?>">HOME</a></li>
            <?php if($catID != 1){?>
            <li>＞</li>
            <li><a href="<?php echo get_category_link( $catPId ); ?>"><?php echo $catPname; ?></a></li>
            <?php }?>
            <li><?php echo $cat->name; ?></li>
        </ul>
    </div><!--bread-->
    <div id="content" class="clearfix">
    
            <div class="p_ttl">
                  <p><img src="<?php bloginfo( 'template_url' );?>/img/topics/page_ttl.png" width="966" height="46" alt="新着情報" /></p>
            </div><!--p_ttl-->

              <div id="side">
                <img class="cate_top" src="<?php bloginfo( 'template_url' );?>/img/topics/cate_top.png" alt=" " width="213" height="9" />
                <ul class="cate">
                <?php
                    $args = array(
                        'show_count' => 0,
                        'hide_empty' => 0,
                        'child_of' => 1,
                        'title_li' => ''
                    );
                    wp_list_categories($args); 
                ?>
                </ul>
                <?php get_sidebar(); ?>
              </div><!--side-->
            <div id="main_cont">
            	<div class="c_ttl"><?php wp_title( '', true, 'right' ); ?></div>
                <article class="box">
                <div class="detail">  
                        <?php if(have_posts()): while(have_posts()): the_post(); ?>
                        <div class="in_box">
                        <span class="title_i"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
                        <span class="data_i"><?php the_time('Y.m.d');?></span>
                        </div>
                        <?php endwhile; else: ?>
                        <div class="not">登録されている記事はありません。</div>
                        <?php endif; ?>
                </div><!--detail-->           
                </article>
                <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); }?> 
            </div><!--main_cont-->
    </div><!--content-->
<?php get_footer(); ?>
</body>
</html>