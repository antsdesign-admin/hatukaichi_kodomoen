<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
<?php add_action ('loop_start', 'needRemoveP'); ?>
    <div id="container">
		<div class="bread clearfix">
			<ul>
				<li><a href="<?php bloginfo( 'url' ); ?>">HOME</a></li>
				<li>＞</li>
				<li><?php the_title(); ?></li>
			</ul>
		</div>
		<!--bread-->
		<div id="content" class="clearfix">
        <?php if(have_posts()): while(have_posts()): the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; endif; ?>
        </div>
    </div>


<?php get_footer(); ?>