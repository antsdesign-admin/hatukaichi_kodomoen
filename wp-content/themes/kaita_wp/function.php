<?php
register_nav_menus(array(
		'main_navigation' => 'Primary Navigation'
	)
);
?>
<?php remove_filter( 'the_content', 'wpautop' ); ?>
<?php remove_filter( 'the_excerpt', 'wpautop' ); ?>