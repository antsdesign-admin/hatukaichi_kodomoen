<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div class="sideBox">
    <a class="opa" href="<?php bloginfo( 'url' ); ?>/enter/"><img class="side_ban_contact" src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban_about.png" width="224" alt="入園について　預かり保育について" /></a>
<?php /*
    <a class="opa videoFancy" data-fancybox href="https://www.youtube.com/watch?v=5iWtLt9v_CI" alt="一日の様子" /><img class="side_ban_top" src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban_about2.png" alt="一日の様子" /></a>
*/?>
    <a class="opa videoFancy" data-fancybox href="<?php bloginfo( 'template_url' ); ?>/video/topmain.mp4" alt="一日の様子" /><img class="side_ban_top" src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban_about2.png" alt="一日の様子" /></a>
<div class="side_ban_area">
    <a class="opa" href="<?php bloginfo( 'url' ); ?>/mind/"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban01.jpg" alt="想いと取り組み"></a>
    <a class="opa" href="<?php bloginfo( 'url' ); ?>/about/equipment"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban02.jpg" alt="施設ご案内" /></a>
    <a class="opa" href="<?php bloginfo( 'url' ); ?>/spend/event"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban03.jpg" alt="年間行事" /></a>
</div><!--side_ban_area-->
<a class="opa" href="<?php bloginfo( 'template_url' ); ?>/img/enter/nyuuen.pdf" target="_blank"><img class="side_ban_top02" src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban_about3.png" alt="入園準備物" /></a>
<a class="opa" href="<?php bloginfo( 'url' ); ?>/recruit/"><img class="side_ban_top02" src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban_about4.png" alt="採用情報" /></a>
<a class="opa" href="<?php bloginfo( 'template_url' ); ?>/img/sports.pdf" target="_blank"><img class="side_ban_top02" src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban_about5.png" alt="スポーツクリエーション（課内・課外）" /></a>
<a href="<?php bloginfo( 'url' ); ?>/links/"><img class="side_ban_top03" src="<?php bloginfo( 'template_url' ); ?>/img/common/side_ban_about6.png" alt="リンク集" /></a>
</div>

