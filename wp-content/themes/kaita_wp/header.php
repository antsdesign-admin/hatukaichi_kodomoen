<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8n" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="viewport" content="target-densitydpi=device-dpi, width=1300, maximum-scale=1.0, user-scalable=yes">
<title>
<?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
//	$site_description = get_bloginfo( 'description', 'display' );
//	if ( $site_description && ( is_home() || is_front_page() ) )
//		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', '' ), max( $paged, $page ) );

	?>
</title>
<?php /*
<meta name="Keywords" content="安芸郡,海田,幼稚園,保育園,園児募集,預かり保育,未就学,楽しい,しつけ,スクールバス" />
<meta name="Description" content="<?php bloginfo('description'); ?>" />
*/ ?>

<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo( 'template_url' ); ?>/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo( 'template_url' ); ?>/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo( 'template_url' ); ?>/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php bloginfo( 'template_url' ); ?>/img/favicon/site.webmanifest">
<link rel="mask-icon" href="<?php bloginfo( 'template_url' ); ?>/img/favicon/safari-pinned-tab.svg" color="#f7ab00">
<meta name="msapplication-TileColor" content="#cde66c">
<meta name="theme-color" content="#cde66c">
<meta name="msapplication-config" content="<?php bloginfo( 'template_url' ); ?>/img/favicon/browserconfig.xml" />
<link rel="icon" type="image/png" sizes="192x192" href="<?php bloginfo( 'template_url' ); ?>/img/favicon/android-chrome-192x192.png">
<link rel="manifest" href="<?php bloginfo( 'template_url' ); ?>/img/favicon/site.webmanifest">

<?php if(is_home()||is_front_page()) {?>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/ fb# website: http://ogp.me/ns/ website#">
<?php } else {?>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/ fb# article: http://ogp.me/ns/ article#">
<?php }?>
<meta property="og:url" content="<?php bloginfo( 'url' ); ?>" />
<meta property="og:type" content="website" />
<meta property="og:title" content="
<?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', '' ), max( $paged, $page ) );

	?>
" />
<meta property="og:description" content="<?php bloginfo('description'); ?>" />
<meta property="og:site_name" content="廿日市こども園" />
<meta property="og:image" content="<?php bloginfo( 'template_url' ); ?>/img/common/ogp.png" />

<link href="<?php bloginfo( 'template_url' ); ?>/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php bloginfo( 'template_url' ); ?>/css/reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php bloginfo( 'template_url' ); ?>/css/common.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php bloginfo( 'template_url' ); ?>/css/nav.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php bloginfo( 'template_url' ); ?>/css/<?php echo getPageName();?>.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php bloginfo( 'template_url' ); ?>/css/jquery.bxslider.css" rel="stylesheet" type="text/css" media="all" />
<script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/fancybox/jquery.fancybox.js"></script> 
<script src="<?php bloginfo( 'template_url' ); ?>/js/lower-jquery.js"></script>
<script src="http://www.google.com/jsapi"></script> 
<!-- <script>google.load("jquery", "1.7");</script>  -->
<?php if(is_home()||is_front_page()) {?>
<script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.bxslider.min.js"></script>
<script>
$(function(){
    
        $('.slider3').bxSlider({
            auto:true,
            speed:3000,
            mode: 'fade',
            captions: false,
            pager: false,
            controls: false
        });
    
});
</script>
<?php }?>
<script src="//feed.mobilesket.com/static/loader.js"></script>
<script>
feedUID = 'RA5BLXos';
feedPrepare.toSmp = {};
feedPrepare.toTab = {};
feedPrepare.subwww = true;
feedPrepare();
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173913403-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-173913403-1');
</script>

<?php wp_head(); ?>
</head>
<body>
<?php if(!is_home()&&!is_front_page()) {?>
<div id="wrapper">
<?php }?>
<header <?php if(!is_home()&&!is_front_page()) {?>id="lower"<?php }?>>
    <div class="h_inner">
        <div id="main_image">
        <?php if(is_home()||is_front_page()) {?>
        <div id="slide">
				<div class="slider3"><img src="<?php bloginfo( 'template_url' ); ?>/img/index/h_main2.png" width="1300" height="524" alt="子どもをみつめ、子どもと学び、子どもと共に。海田幼稚園、海田保育園、子どもたちにとって満足のいくあそびと学びの場を" /> <img src="<?php bloginfo( 'template_url' ); ?>/img/index/h_main3.png" width="1300" height="524" alt="子どもをみつめ、子どもと学び、子どもと共に。海田幼稚園、海田保育園、子どもたちにとって満足のいくあそびと学びの場を" /> </div>
				<!--slider3--> 
			</div>
            <?php }else {?>
            <h1><img src="<?php bloginfo( 'template_url' ); ?>/img/common/main_img.png" alt="子どもをみつめ、子どもと学び、子どもと共に。海田幼稚園、海田保育園" usemap="#Map"  />
              <map name="Map" id="Map">
                <area shape="rect" coords="160,55,649,98" href="<?php bloginfo( 'url' ); ?>" alt="" />
              </map>
            </h1>
            <?php }?>
        </div>
        <nav id="g_nav">
            <ul>
                <li class="nav01<?php if(is_home()||is_front_page()){ echo ' on'; } ?>"><a href="<?php bloginfo( 'url' ); ?>">HOME</a></li>
                <li class="nav02<?php if(is_page('mind')||is_page('issues')){ echo ' on'; } ?>"><a href="<?php bloginfo( 'url' ); ?>/mind/">想いと取り組み</a></li>
                <li class="nav03<?php if(is_page(array('community','about','access','equipment','staff'))){ echo ' on'; } ?>"><a href="<?php bloginfo( 'url' ); ?>/about">学園の紹介</a></li>
                <li class="nav04<?php if(is_page('spend')||is_page('event')){ echo ' on'; } ?>"><a href="<?php bloginfo( 'url' ); ?>/spend/">園での過ごし方</a></li>
                <li class="nav05<?php if(is_single()||is_category()){ echo ' on';} ?>"><a href="<?php bloginfo( 'url' ); ?>/information">新着情報</a></li>
                <li class="nav06<?php if(is_page('enter')){ echo ' on'; } ?>"><a href="<?php bloginfo( 'url' ); ?>/enter/">入園について</a></li>
                <li class="nav07<?php if(is_page('recruit')){ echo ' on'; } ?>"><a href="<?php bloginfo( 'url' ); ?>/recruit/">採用情報</a></li>
                <li class="nav08<?php if(is_page('contact')){ echo ' on'; } ?>"><a href="<?php bloginfo( 'url' ); ?>/contact/">お問い合わせ</a></li>
            </ul>
        </nav>
    </div>
    <!--h_inner-->
</header>

