<footer>
	<div class="f_inner">
		<div class="f_top"> <a class="opa" href="<?php bloginfo( 'url' ); ?>/contact/"><img class="ban_contact" src="<?php bloginfo( 'template_url' ); ?>/img/common/ban_contact.png" width="213" height="61" alt="お問合せはこちらから" /></a> <span class="ex">ご不明な点やお問い合わせ等、お気軽にご連絡ください。お電話でも承っております。</span> <a href="#"><img class="toTop" src="<?php bloginfo( 'template_url' ); ?>/img/common/toTop.png" width="55" height="32" alt="ページのトップへ戻る" /></a> </div>
		<!--f_top-->
		<div class="f_bottom clearfix">
			<div class="inner">
				<div class="logo">
					<h1><img src="<?php bloginfo( 'template_url' ); ?>/img/common/f_logo.png" width="201" height="131" alt="海田幼稚園　海田保育園" /></h1>
					<p>〒738-0005<br>
						広島県廿日市市桜尾本町2-19-5<br>
						TEL 0829-31-5635<br>
						FAX 0829-31-5631</p>
				</div>
				<!--logo-->
				<div class="sitemap">
					<ul class="left">
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/">HOME</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/information">新着情報</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/contact/">お問い合わせ</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/recruit/">採用情報</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/enter/">入園について</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/links/">リンク集</a></li>
					</ul>
					<ul class="center">
						<li>想いと取り組み</li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/mind/">理念･教育･保育目標</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/mind/issues">4つの取り組み</a></li>
						<br />
						<li>園での過ごし方</li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/spend/">一日の過ごし方</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/spend/event">年間行事</a></li>
					</ul>
					<ul class="right">
						<li>学園の紹介</li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/about/">学園の概要</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/about/community">一時保育･課外授業</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/about/equipment">施設案内</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/about/staff">教員紹介</a></li>
						<li><span class="green">●</span> <a href="<?php bloginfo( 'url' ); ?>/about/access">交通･アクセス</a></li>
					</ul>
				</div>
				<!--sitemap-->
				<div class="f_ban"> <a class="opa" href="http://www.hda.ac.jp/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/f_ban01.jpg" alt="学校法人三宅学園のサイトはこちらから" /></a> <a class="opa" href="http://www.hda.ac.jp/kaita/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/f_ban02.jpg" alt="海田幼稚園・海田保育園" /></a> <a class="opa" href="http://www.hda.ac.jp/academy/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/f_ban03.jpg" alt="広島デンタルアカデミーのサイトはこちらから" /></a> <a class="opa" href="http://www.hda.ac.jp/kudamatsu/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/f_ban04.jpg" alt="下松デンタルアカデミー専門学校" /></a> <a class="opa" href="http://www.hda.ac.jp/clinic/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/img/common/f_ban05.jpg" alt="海田デンタルクリニックのサイトはこちらから" /></a> </div>
				<!--f_ban--> 
			</div>
			<!--inner--> 
		</div>
		<!--f_bottom--> 
	</div>
	<!--f_inner-->
	<div class="copy"> Copyright &copy;  Hatukaichi Kindergarten_nursery school all rights reserved. </div>
	<!--copy--> 
</footer>
<?php wp_footer(); ?>
<?php if(!is_home()&&!is_front_page()) {?>
</div><!--wrapper-->
<?php }?>
