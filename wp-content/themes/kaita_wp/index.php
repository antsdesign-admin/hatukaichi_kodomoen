<?php get_header(); ?>
<div id="wrapper">
	<div id="container" class="clearfix">
		<div class="bottom_area">
			<ul class="c_nav clearfix">
				<a class="opa" href="<?php bloginfo( 'url' ); ?>/mind/">
				<li>
					<div class="box bg01">
						<p>廿日市こども園の想いと<br>
							取り組みをご紹介いたします。 <img src="<?php bloginfo( 'template_url' ); ?>/img/index/ph01.jpg" width="186" height="125" alt="想いと取り組み" /></p>
					</div>
					<!--box--> 
				</li>
				</a> <a class="opa" href="<?php bloginfo( 'url' ); ?>/spend/event">
				<li>
					<div class="box bg02">
						<p>季節の移り変わりを感じながら<br>
							様々な行事が体験できます。 <img src="<?php bloginfo( 'template_url' ); ?>/img/index/ph02.jpg" width="186" height="125" alt="年間行事" /></p>
					</div>
					<!--box--> 
				</li>
				</a> <a class="opa" href="<?php bloginfo( 'url' ); ?>/about/equipment">
				<li>
					<div class="box bg03">
						<p>安心・安全に楽しく過ごせる<br>
							施設や環境が整っています。 <img src="<?php bloginfo( 'template_url' ); ?>/img/index/ph03.jpg" width="186" height="125" alt="施設ご案内" /> </p>
					</div>
					<!--box--> 
				</li>
				</a>
			</ul>
            <?php query_posts('showposts=8&post_type=post');
            if(have_posts()): ?> 
            <div class="info">
                <div class="topi_ttl">
                    <img src="<?php bloginfo( 'template_url' ); ?>/img/index/topi_ccl.jpg" alt="新着情報" width="91" height="22">
                </div>
                <div class="go_list"><a href="<?php bloginfo( 'url' ); ?>/information" target="_parent"><img src="<?php bloginfo( 'template_url' ); ?>/img/index/goList.jpg" alt="一覧を見る"></a></div>
                <ul>
                <?php while (have_posts()) : the_post();?>
                    <li>
                        <span class="title"><a href="<?php the_permalink(); ?>" target="_parent"><?php the_title(); ?></a></span><span class="data"><?php the_time('Y.m.d'); ?></span>
                    </li>
                <?php endwhile;?>
                </ul>
            </div>
        </div>
        <?php endif;wp_reset_query();?>
		<!--bottom_area-->
		<div class="side_ban"> <a class="opa" href="<?php bloginfo( 'url' ); ?>/enter"><img src="<?php bloginfo( 'template_url' ); ?>/img/index/ban_about.png" alt="入園について" /></a><br />
			<a class="opa videoFancy" data-fancybox href="https://www.youtube.com/watch?v=5iWtLt9v_CI"><img class="side_ban_top" src="<?php bloginfo( 'template_url' ); ?>/img/index/ban_about2.png" alt="預かり保育について" /></a> <a class="opa" href="<?php bloginfo( 'template_url' ); ?>/img/enter/nyuuen.pdf" target="_blank"><img class="side_ban_top02" src="<?php bloginfo( 'template_url' ); ?>/img/index/ban_about3.png" alt="入園準備物" /></a> <a class="opa" href="<?php bloginfo( 'url' ); ?>/recruit/"><img class="side_ban_top02" src="<?php bloginfo( 'template_url' ); ?>/img/index/ban_about4.png" alt="採用情報" /></a> 
			<a class="opa" href="<?php bloginfo( 'template_url' ); ?>/img/sports.pdf" target="_blank"><img class="side_ban_top02" src="<?php bloginfo( 'template_url' ); ?>/img/index/ban_about5.png" alt="スポーツクリエーション（課内・課外）" /></a> <a href="<?php bloginfo( 'url' ); ?>/links/"><img class="side_ban_top03" src="<?php bloginfo( 'template_url' ); ?>/img/index/ban_about6.png" alt="リンク集" /></a> </div>
		<!--side_ban--> 
	</div>
	<!--container--> 
</div>
<!--wrapper-->
<?php get_footer(); ?>
